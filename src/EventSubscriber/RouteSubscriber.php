<?php

declare(strict_types=1);

namespace Drupal\umass_azure_openid\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Initializes an instance of the route subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    // Deny access to '/user/password' for anonymous users
    // Allow authenticated users to change their own password
    // Note that the second parameter of setRequirement() is a string.
    if ($route = $collection->get('user.pass')) {
      if (!$this->currentUser->isAuthenticated()) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
